// main.js

// jQuery para mostrar/ocultar el menú de la barra lateral en dispositivos móviles
$(document).ready(function(){
  // Manejar el clic en el botón de alternar la barra lateral
  $('#sidebarToggle').click(function(){
    // Alternar la clase 'show' en el menú de la barra lateral
    $('nav.sidebar-nav').toggleClass('show');
  });
});

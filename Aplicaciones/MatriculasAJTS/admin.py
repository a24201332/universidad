from django.contrib import admin
from .models import Carrera
from .models import Asignatura
from .models import Curso
from .models import Matricula
# Register your models here.
@admin.register(Carrera)
class CarreraAdmin(admin.ModelAdmin):
    list_display=('idCarreraAJTS', 'nombreCarreraAJTS', 'directorCarreraAJTS', 'logoCarreraAJTS', 'facultadCarreraAJTS', 'duracionCarreraAJTS')
    search_fields= ('idCarreraAJTS','nombreCarreraAJTS','facultadCarreraAJTS')
    list_editable=('nombreCarreraAJTS', 'directorCarreraAJTS', 'logoCarreraAJTS', 'facultadCarreraAJTS', 'duracionCarreraAJTS')
    list_filter=('nombreCarreraAJTS',)
    list_per_page = 3
admin.site.register(Asignatura)
admin.site.register(Curso)
admin.site.register(Matricula)

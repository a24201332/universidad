from django.urls import path
from . import views
from .views import listadoMatricula

urlpatterns=[
path('', views.inicio, name='inicio'),
#33333333333333333333333333333333
path('asignatura/',views.listadoAsignatura),
path('guardarAsignatura/',views.guardarAsignatura),
path('eliminarAsignatura/<idAsignaturaAJTS>',views.eliminarAsignatura),
path('editarAsignatura/<idAsignaturaAJTS>',views.editarAsignatura),
path('procesarActualizacionAsignatura/<idAsignaturaAJTS>',views.procesarActualizacionAsignatura, name='procesarActualizacionAsignatura'),
#55455555555555555555555555
path('carrera/',views.listadoCarrera),
path('guardarCarrera/',views.guardarCarrera),
path('eliminarCarrera/<idCarreraAJTS>',views.eliminarCarrera),
path('editarCarrera/<idCarreraAJTS>',views.editarCarrera),
path('procesarActualizacionCarrera/<idCarreraAJTS>',views.procesarActualizacionCarrera, name='procesarActualizacionCarrera'),
#55455555555555555555555555
path('curso/',views.listadoCurso),
path('guardarCurso/',views.guardarCurso),
path('eliminarCurso/<idCursoAJTS>',views.eliminarCurso),
path('editarCurso/<idCursoAJTS>',views.editarCurso),
path('procesarActualizacionCurso/<idCursoAJTS>',views.procesarActualizacionCurso, name='procesarActualizacionCurso'),
#55455555555555555555555555
path('matricula/', views.listadoMatricula),
path('guardarMatricula/',views.guardarMatricula),
path('eliminarMatricula/<idMatriculaAJTS>',views.eliminarMatricula),
path('editarMatricula/<idMatriculaAJTS>',views.editarMatricula),
path('procesarActualizacionMatricula/<idMatriculaAJTS>',views.procesarActualizacionMatricula, name='procesarActualizacionMatricula'),
#55455555555555555555555555
path('vista1',views.vista1,name='vista1'),
path('enviar_correo/',views.enviar_correo, name='enviar_correo')
]

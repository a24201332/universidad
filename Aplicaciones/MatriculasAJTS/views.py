from django.shortcuts import render, redirect
from .models import Asignatura
from .models import Carrera
from .models import Curso
from .models import Matricula
from django.contrib import messages
from django.conf import settings
from django.core.mail import send_mail
from django.http import HttpResponseRedirect



# Create your views here.


def inicio(request):
    return render(request, 'inicio.html')


def listadoAsignatura(request):
    asignaturasBdd =Asignatura.objects.all()
    return render(request, 'asignatura.html',{'asignaturas':asignaturasBdd})


def guardarAsignatura(request):
    nombreEstudianteAJTS = request.POST["nombreEstudianteAJTS"]
    nombreAsignaturaAJTS = request.POST["nombreAsignaturaAJTS"]
    creditosAsignaturaAJTS = request.POST["creditosAsignaturaAJTS"]
    fechaInicioAsignaturaAJTS = request.POST["fechaInicioAsignaturaAJTS"]
    fechaFinalizacionAsignaturaAJTS = request.POST["fechaFinalizacionAsignaturaAJTS"]
    profesorAsignaturaAJTS = request.POST["profesorAsignaturaAJTS"]
    silaboAsignaturaAJTS=request.FILES.get("silaboAsignaturaAJTS")
    requisitosAsignaturaAJTS = request.POST["requisitosAsignaturaAJTS"]
    descripcionAsignaturaAJTS = request.POST["descripcionAsignaturaAJTS"]

    # Formatear la fecha correctamente


    # Insertando datos mediante ORM de Django
    nuevoAsignatura = Asignatura.objects.create(
        nombreEstudianteAJTS=nombreEstudianteAJTS,
        nombreAsignaturaAJTS=nombreAsignaturaAJTS,
        creditosAsignaturaAJTS=creditosAsignaturaAJTS,
        fechaInicioAsignaturaAJTS=fechaInicioAsignaturaAJTS,
        fechaFinalizacionAsignaturaAJTS=fechaFinalizacionAsignaturaAJTS,
        profesorAsignaturaAJTS=profesorAsignaturaAJTS,
        silaboAsignaturaAJTS=silaboAsignaturaAJTS,
        requisitosAsignaturaAJTS=requisitosAsignaturaAJTS,
        descripcionAsignaturaAJTS=descripcionAsignaturaAJTS
    )

    messages.success(request, 'Haz sido Matriculado exitosamente ')
    return redirect('/asignatura/')


def eliminarAsignatura(request, idAsignaturaAJTS):
    asignaturaEliminar = Asignatura.objects.get(idAsignaturaAJTS=idAsignaturaAJTS)
    asignaturaEliminar.delete()
    messages.error(request, 'Eliminado la Matriculado exitosamente ')
    return redirect('/asignatura/')


def editarAsignatura(request, idAsignaturaAJTS):
    asignaturaEditar= Asignatura.objects.get(idAsignaturaAJTS=idAsignaturaAJTS)
    return render(request, 'editarAsignatura.html', {'asignatura': asignaturaEditar})


def procesarActualizacionAsignatura(request, idAsignaturaAJTS):
    idAsignaturaAJTS = request.POST["idAsignaturaAJTS"]
    nombreEstudianteAJTS = request.POST["nombreEstudianteAJTS"]
    nombreAsignaturaAJTS = request.POST["nombreAsignaturaAJTS"]
    creditosAsignaturaAJTS = request.POST["creditosAsignaturaAJTS"]
    fechaInicioAsignaturaAJTS = request.POST["fechaInicioAsignaturaAJTS"]
    fechaFinalizacionAsignaturaAJTS = request.POST["fechaFinalizacionAsignaturaAJTS"]
    profesorAsignaturaAJTS = request.POST["profesorAsignaturaAJTS"]
    silaboAsignaturaAJTS=request.FILES.get("silaboAsignaturaAJTS")
    requisitosAsignaturaAJTS = request.POST["requisitosAsignaturaAJTS"]
    descripcionAsignaturaAJTS = request.POST["descripcionAsignaturaAJTS"]


    # Insertando datos mediante ORM de Django
    asignaturaEditar = Asignatura.objects.get(idAsignaturaAJTS=idAsignaturaAJTS)
    asignaturaEditar.nombreEstudianteAJTS = nombreEstudianteAJTS
    asignaturaEditar.nombreAsignaturaAJTS = nombreAsignaturaAJTS
    asignaturaEditar.creditosAsignaturaAJTS = creditosAsignaturaAJTS
    asignaturaEditar.fechaInicioAsignaturaAJTS = fechaInicioAsignaturaAJTS
    asignaturaEditar.fechaFinalizacionAsignaturaAJTS = fechaFinalizacionAsignaturaAJTS
    asignaturaEditar.profesorAsignaturaAJTS = profesorAsignaturaAJTS
    asignaturaEditar.silaboAsignaturaAJTS = silaboAsignaturaAJTS
    asignaturaEditar.requisitosAsignaturaAJTS = requisitosAsignaturaAJTS
    asignaturaEditar.descripcionAsignaturaAJTS = descripcionAsignaturaAJTS
    asignaturaEditar.save()

    messages.success(request, 'Editado la Matriculado exitosamente ')
    return redirect('/asignatura/')


#333333333333333333333333333333333333333333

def listadoCarrera(request):
    carrerasBdd =Carrera.objects.all()
    return render(request, 'carrera.html',{'carreras':carrerasBdd})

def guardarCarrera(request):
    nombreCarreraAJTS = request.POST["nombreCarreraAJTS"]
    directorCarreraAJTS = request.POST["directorCarreraAJTS"]
    logoCarreraAJTS=request.FILES.get("logoCarreraAJTS")
    facultadCarreraAJTS = request.POST["facultadCarreraAJTS"]
    duracionCarreraAJTS = request.POST["duracionCarreraAJTS"]

    # Formatear la fecha correctamente


    # Insertando datos mediante ORM de Django
    nuevoCarrera = Carrera.objects.create(
        nombreCarreraAJTS=nombreCarreraAJTS,
        directorCarreraAJTS=directorCarreraAJTS,
        logoCarreraAJTS=logoCarreraAJTS,
        facultadCarreraAJTS=facultadCarreraAJTS,
        duracionCarreraAJTS=duracionCarreraAJTS
    )

    messages.success(request, 'Carrera guardada exitosamente')
    return redirect('/carrera/')



def eliminarCarrera(request, idCarreraAJTS):
    carreraEliminar = Carrera.objects.get(idCarreraAJTS=idCarreraAJTS)
    carreraEliminar.delete()
    messages.error(request, 'Carrera eliminado exitosamente')
    return redirect('/carrera/')


def editarCarrera(request, idCarreraAJTS):
    carreraEditar= Carrera.objects.get(idCarreraAJTS=idCarreraAJTS)
    return render(request, 'editarCarrera.html', {'carrera': carreraEditar})


def procesarActualizacionCarrera(request, idCarreraAJTS):
    idCarreraAJTS = request.POST["idCarreraAJTS"]
    nombreCarreraAJTS = request.POST["nombreCarreraAJTS"]
    directorCarreraAJTS = request.POST["directorCarreraAJTS"]
    logoCarreraAJTS=request.FILES.get("logoCarreraAJTS")
    facultadCarreraAJTS = request.POST["facultadCarreraAJTS"]
    duracionCarreraAJTS = request.POST["duracionCarreraAJTS"]



    # Insertando datos mediante ORM de Django
    carreraEditar = Carrera.objects.get(idCarreraAJTS=idCarreraAJTS)
    carreraEditar.nombreCarreraAJTS = nombreCarreraAJTS
    carreraEditar.directorCarreraAJTS = directorCarreraAJTS
    carreraEditar.logoCarreraAJTS = logoCarreraAJTS
    carreraEditar.facultadCarreraAJTS = facultadCarreraAJTS
    carreraEditar.duracionCarreraAJTS = duracionCarreraAJTS


    messages.success(request, 'Carrera ACTUALIZADO Exitosamente')
    return redirect('/carrera/')
#333333333333333333333333333333333333333333
def listadoCurso(request):
    cursosBdd = Curso.objects.all()
    carrerasBdd = Carrera.objects.all()
    return render(request, 'curso.html', {'cursos': cursosBdd, 'carreras': carrerasBdd})
    

def guardarCurso(request):
    idCarreraAJTS_nombreCarreraAJTS = request.POST["carrera"]
    carreraSeleccionado = Carrera.objects.get(idCarreraAJTS=idCarreraAJTS_nombreCarreraAJTS)
    nivelCursoAJTS = request.POST["nivelCursoAJTS"]
    descripcionCursoAJTS = request.POST["descripcionCursoAJTS"]
    aulaCursoAJTS = request.POST["aulaCursoAJTS"]
    paraleloCursoAJTS = request.POST["paraleloCursoAJTS"]

    nuevoCurso = Curso.objects.create(
        nombreCarreraAJTS=carreraSeleccionado,
        nivelCursoAJTS=nivelCursoAJTS,
        descripcionCursoAJTS=descripcionCursoAJTS,
        aulaCursoAJTS=aulaCursoAJTS,
        paraleloCursoAJTS=paraleloCursoAJTS
    )

    messages.success(request, 'Curso guardado exitosamente')
    return redirect('/curso/')


def eliminarCurso(request, idCursoAJTS):
    cursoEliminar = Curso.objects.get(idCursoAJTS=idCursoAJTS)
    cursoEliminar.delete()
    messages.error(request, 'Curso eliminado exitosamente')
    return redirect('/curso/')

def editarCurso(request, idCursoAJTS):
    cursoEditar = Curso.objects.get(idCursoAJTS=idCursoAJTS)
    carrerasBdd = Carrera.objects.all()
    return render(request, 'editarCurso.html', {'curso': cursoEditar, 'carreras': carrerasBdd})

def procesarActualizacionCurso(request, idCursoAJTS):
    idCarreraAJTS_nombreCarreraAJTS = request.POST["carrera"]
    carreraSeleccionado = Carrera.objects.get(idCarreraAJTS=idCarreraAJTS_nombreCarreraAJTS)
    nivelCursoAJTS = request.POST["nivelCursoAJTS"]
    descripcionCursoAJTS = request.POST["descripcionCursoAJTS"]
    aulaCursoAJTS = request.POST["aulaCursoAJTS"]
    paraleloCursoAJTS = request.POST["paraleloCursoAJTS"]

    cursoEditar = Curso.objects.get(idCursoAJTS=idCursoAJTS)
    cursoEditar.nivelCursoAJTS = nivelCursoAJTS
    cursoEditar.descripcionCursoAJTS = descripcionCursoAJTS
    cursoEditar.aulaCursoAJTS = aulaCursoAJTS
    cursoEditar.paraleloCursoAJTS = paraleloCursoAJTS
    cursoEditar.carrera = carreraSeleccionado
    cursoEditar.save()

    messages.success(request, 'Curso actualizado exitosamente')
    return redirect('/curso/')

#33333333333333333333333333333333

def listadoMatricula(request):
    matriculasBdd = Matricula.objects.all()
    carrerasBdd = Carrera.objects.all()
    cursosBdd = Curso.objects.all()
    asignaturasBdd =Asignatura.objects.all()
    return render(request, 'matricula.html', {'matriculas': matriculasBdd,  'carreras': carrerasBdd, 'cursos': cursosBdd, 'asignaturas': asignaturasBdd})



def guardarMatricula(request):
    idAsignaturaAJTS_nombreEstudianteAJTS = request.POST["asignatura"]
    estudianteSeleccionado = Asignatura.objects.get(idAsignaturaAJTS=idAsignaturaAJTS_nombreEstudianteAJTS)

    idAsignaturaAJTS_nombreAsignaturaAJTS = request.POST["asignatura"]
    asignaturaSeleccionado = Asignatura.objects.get(idAsignaturaAJTS=idAsignaturaAJTS_nombreAsignaturaAJTS)

    idAsignaturaAJTS_fechaInicioAsignaturaAJTS = request.POST["asignatura"]
    fechaInicioSeleccionado = Asignatura.objects.get(idAsignaturaAJTS=idAsignaturaAJTS_fechaInicioAsignaturaAJTS)

    idAsignaturaAJTS_fechaFinalizacionAsignaturaAJTS = request.POST["asignatura"]
    fechaFinalizacionSeleccionado = Asignatura.objects.get(idAsignaturaAJTS=idAsignaturaAJTS_fechaFinalizacionAsignaturaAJTS)

    idCarreraAJTS_nombreCarreraAJTS = request.POST["carrera"]
    carreraSeleccionado = Carrera.objects.get(idCarreraAJTS=idCarreraAJTS_nombreCarreraAJTS)

    idCursoAJTS_nivelCursoAJTS = request.POST["nivel"]
    nivelSeleccionado = Curso.objects.get(idCursoAJTS=idCursoAJTS_nivelCursoAJTS )

    idCursoAJTS_paraleloCursoAJTS = request.POST["paralelo"]
    paraleloSeleccionado = Curso.objects.get(idCursoAJTS=idCursoAJTS_paraleloCursoAJTS)


    # Insertando datos mediante ORM de Django

    nuevoMatricula = Matricula.objects.create(
        nombreEstudianteAJTS=estudianteSeleccionado,
        nombreAsignaturaAJTS=asignaturaSeleccionado,
        fechaInicioAsignaturaAJTS=fechaInicioSeleccionado,
        fechaFinalizacionAsignaturaAJTS=fechaFinalizacionSeleccionado,
        nombreCarreraAJTS=carreraSeleccionado,
        nivelCursoAJTS=nivelSeleccionado,
        paraleloCursoAJTS=paraleloSeleccionado,
    )

    messages.success(request, 'Ha sido Matriculado satistafactoriamente')
    return redirect('/matricula/')


def eliminarMatricula(request, idMatriculaAJTS):
    matriculaEliminar = Matricula.objects.get(idMatriculaAJTS=idMatriculaAJTS)
    matriculaEliminar.delete()
    messages.error(request, 'Matricula eliminado exitosamente')
    return redirect('/matricula/')

def editarMatricula(request, idMatriculaAJTS):
    matriculaEditar = Matricula.objects.get(idMatriculaAJTS=idMatriculaAJTS)
    carrerasBdd = Carrera.objects.all()
    cursosBdd = Curso.objects.all()
    asignaturasBdd =Asignatura.objects.all()
    return render(request, 'editarMatricula.html', {'matricula': matriculaEditar, 'carreras': carrerasBdd, 'cursos': cursosBdd, 'asignaturas': asignaturasBdd})

def procesarActualizacionMatricula(request, idMatriculaAJTS):
    idAsignaturaAJTS_nombreEstudianteAJTS = request.POST["asignatura"]
    estudianteSeleccionado = Asignatura.objects.get(idAsignaturaAJTS=idAsignaturaAJTS_nombreEstudianteAJTS)

    idAsignaturaAJTS_nombreAsignaturaAJTS = request.POST["asignatura"]
    asignaturaSeleccionado = Asignatura.objects.get(idAsignaturaAJTS=idAsignaturaAJTS_nombreAsignaturaAJTS)

    idAsignaturaAJTS_fechaInicioAsignaturaAJTS = request.POST["asignatura"]
    fechaInicioSeleccionado = Asignatura.objects.get(idAsignaturaAJTS=idAsignaturaAJTS_fechaInicioAsignaturaAJTS)

    idAsignaturaAJTS_fechaFinalizacionAsignaturaAJTS = request.POST["asignatura"]
    fechaFinalizacionSeleccionado = Asignatura.objects.get(idAsignaturaAJTS=idAsignaturaAJTS_fechaFinalizacionAsignaturaAJTS)

    idCarreraAJTS_nombreCarreraAJTS = request.POST["carrera"]
    carreraSeleccionado = Carrera.objects.get(idCarreraAJTS=idCarreraAJTS_nombreCarreraAJTS)

    idCursoAJTS_nivelCursoAJTS = request.POST["nivel"]
    nivelSeleccionado = Curso.objects.get(idCursoAJTS=idCursoAJTS_nivelCursoAJTS )

    idCursoAJTS_paraleloCursoAJTS = request.POST["paralelo"]
    paraleloSeleccionado = Curso.objects.get(idCursoAJTS=idCursoAJTS_paraleloCursoAJTS)


    matriculaEditar = Matricula.objects.get(idMatriculaAJTS=idMatriculaAJTS)
    matriculaEditar.asignatura = estudianteSeleccionado
    matriculaEditar.asignatura = asignaturaSeleccionado
    matriculaEditar.asignatura = fechaInicioSeleccionado
    matriculaEditar.asignatura = fechaFinalizacionSeleccionado
    matriculaEditar.carrera = nivelSeleccionado
    matriculaEditar.curso = nivelSeleccionado
    matriculaEditar.curso = paraleloSeleccionado


    matriculaEditar.save()

    messages.success(request, 'Matricula editado exitosamente')
    return redirect('/matricula/')


#33333333333333333333333333333333

def vista1(request):
    return render(request, 'enviar_correo.html')


def enviar_correo(request):
    if request.method =='POST':
        destinatario = request.POST.get('destinatario')
        asunto = request.POST.get('asunto')
        cuerpo = request.POST.get('cuerpo')
        send_mail(asunto, cuerpo, settings.EMAIL_HOST_USER, [destinatario], fail_silently=False)

        messages.success(request, 'SE HA ENVIADO EL CORREO EXISTOSAMENTE')
        return HttpResponseRedirect('/vista1')


    return render(request, 'enviar_correo.html')

# Generated by Django 4.2.7 on 2024-01-28 19:21

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AsignaturaAJTS',
            fields=[
                ('idAsignaturaAJTS', models.AutoField(primary_key=True, serialize=False)),
                ('nombreAsignaturaAJTS', models.CharField(max_length=150)),
                ('creditosAsignaturaAJTS', models.TextField()),
                ('fechaInicioAsignaturaAJTS', models.DateField()),
                ('fechaFinalizacionAsignaturaAJTS', models.DateField()),
                ('profesorAsignaturaAJTS', models.CharField(max_length=150)),
                ('silaboAsignaturaAJTS', models.FileField(blank=True, null=True, upload_to='asignaturas')),
                ('descripcionAsignaturaAJTS', models.TextField()),
                ('calificacionAsignaturaAJTS', models.DecimalField(blank=True, decimal_places=2, max_digits=5, null=True)),
            ],
        ),
    ]

from django.db import models


class Carrera(models.Model):
    idCarreraAJTS = models.AutoField(primary_key=True)
    nombreCarreraAJTS = models.CharField(max_length=150)
    directorCarreraAJTS = models.CharField(max_length=150)
    logoCarreraAJTS = models.FileField(upload_to='carreras', null=True, blank=True)
    facultadCarreraAJTS = models.CharField(max_length=150)
    duracionCarreraAJTS = models.CharField(max_length=150)


    def __str__(self):
        fila = "{0}: {1} - {2} a {3}"
        return fila.format(self.idCarreraAJTS, self.nombreCarreraAJTS, self.directorCarreraAJTS, self.logoCarreraAJTS)

class Curso(models.Model):
    idCursoAJTS = models.AutoField(primary_key=True)
    nivelCursoAJTS = models.CharField(max_length=150)
    descripcionCursoAJTS = models.CharField(max_length=150)
    aulaCursoAJTS = models.CharField(max_length=150)
    paraleloCursoAJTS = models.CharField(max_length=10, blank=True)
    nombreCarreraAJTS= models.ForeignKey(Carrera, null=True, blank=True, on_delete=models.PROTECT)

    def __str__(self):
        texto = "{0} ({1})"
        return texto.format(self.nombre, self.creditos)


    def __str__(self):
        fila = "{0}: {1} - {2} a {3}"
        return fila.format(self.idCursoAJTS, self.nivelCursoAJTS, self.descripcionCursoAJTS, self.aulaCursoAJTS)


class Asignatura(models.Model):
    idAsignaturaAJTS = models.AutoField(primary_key=True)
    nombreEstudianteAJTS = models.CharField(max_length=150)
    nombreAsignaturaAJTS = models.CharField(max_length=150)
    creditosAsignaturaAJTS = models.TextField()
    fechaInicioAsignaturaAJTS = models.DateField()
    fechaFinalizacionAsignaturaAJTS = models.DateField()
    profesorAsignaturaAJTS = models.CharField(max_length=150)
    silaboAsignaturaAJTS = models.FileField(upload_to='asignaturas', null=True, blank=True)
    requisitosAsignaturaAJTS = models.CharField(max_length=150)
    descripcionAsignaturaAJTS = models.TextField()


    def __str__(self):
        fila = "{0}: {1} - {2} a {3}"
        return fila.format(self.idAsignaturaAJTS, self.nombreEstudianteAJTS, self.nombreAsignaturaAJTS, self.fechaInicioAsignaturaAJTS, self.fechaFinalizacionAsignaturaAJTS)

class Matricula(models.Model):
    idMatriculaAJTS = models.AutoField(primary_key=True)
    nombreEstudianteAJTS = models.ForeignKey(Asignatura, on_delete=models.PROTECT, related_name='matriculaEstudiante')
    nombreAsignaturaAJTS = models.ForeignKey(Asignatura, on_delete=models.PROTECT, related_name='matriculaAsignatura')
    fechaInicioAsignaturaAJTS = models.ForeignKey(Asignatura, on_delete=models.PROTECT, related_name='matriculaFechaInicio')
    fechaFinalizacionAsignaturaAJTS = models.ForeignKey(Asignatura, on_delete=models.PROTECT, related_name='matriculafechaFinalizacion')
    nombreCarreraAJTS = models.ForeignKey(Carrera, on_delete=models.PROTECT, related_name='matriculaCarrera')
    nivelCursoAJTS = models.ForeignKey(Curso, on_delete=models.PROTECT, related_name='matriculaNivel')
    paraleloCursoAJTS = models.ForeignKey(Curso, on_delete=models.PROTECT, related_name='matriculaParalelo')
